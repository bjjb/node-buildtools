utils = require './utils'
{
  flatten
  glob
  mkdir
  read
  rm
  stat
  uniq
  write
} = utils

plugins = require './plugins'
{ 
  html
  css
  js
  coffee
  stylus
  pug
} = plugins

runners = require './runners'
{ 
  build
  serve
  clean
  watch
} = runners

src = require './src'
dest = require './dest'
pipe = require './pipe'

module.exports = {
  utils

  flatten
  glob
  mkdir
  read
  rm
  stat
  uniq
  write

  plugins

  html
  css
  js
  coffee
  stylus
  pug

  runners

  build
  serve
  clean
  watch

  src
  dest
  pipe
}
