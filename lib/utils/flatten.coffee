reducer = (a, b) ->
  b = b.reduce(reducer, []) if b and typeof b.reduce is 'function'
  a.concat(b)

flatten = (array) ->
  throw 'expected an array' unless array instanceof Array
  array.reduce reducer, []

module.exports = flatten
