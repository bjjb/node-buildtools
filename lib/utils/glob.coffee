{ promisify } = require 'util'
glob = promisify(require('glob'))
module.exports = glob
