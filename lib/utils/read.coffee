{ promisify } = require 'util'
{ readFile } = require 'fs'

# Gets a promise to read the contents of a file
readFile = promisify(readFile)

# Gets a promise to expand a glob to an array of pathnames
glob = require './glob'

# Reduce function to flatten out an array which might contain nested arrays
flattenReducer = (a, v) -> a.concat(v.reduce?(flattenReducer, []) ? v)

# Flattens an array
flatten = (a) -> a.reduce(flattenReducer, [])

# Filter function to remove duplicates from an array
uniqueReducer = (a, v) -> if v in a then a else a.concat(v)

# Uniqueifies all values in an array
unique = (a) -> a.reduce(uniqueReducer, [])

# Takes an array of globs and expands them. Returns a promise which resolves
# to the flat array of paths.
reduce = (g) -> Promise.all(g.map((g) -> glob(g))).then(flatten).then(unique)

# Gets a function to join an array of strings with the joinChar
join = (joinChar) -> (strings) -> strings.join(joinChar)

# Gets a promise to read the contents of the utf8-encoded file at path
readOne = (path) -> readFile path, 'utf8' 

# Reads all files specified by all globs and joins them together with
# newlines. If the order is important, then specify the important files first,
# followed by one or more globs.
read = (globs...) ->
  reduce(globs).then (paths) ->
    switch paths.length
      when 0 then return Promise.resolve(null)
      when 1 then return readOne(paths[0])
      else Promise.all(paths.map(readOne)).then(join("\n"))

module.exports = read
