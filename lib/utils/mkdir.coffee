mktree = require 'mkdir-p'

# Returns a promise which resolves to the name of the directory if it was
# actually made. Will also make parent directories.
mkdir = (paths...) ->
  operations = paths.map (path) ->
    new Promise (resolve, reject) ->
      mktree path, (err) ->
        return reject(err) unless err?.code is 'EEXIST' if err?
        resolve(path)
  Promise.all(operations)

module.exports = mkdir
