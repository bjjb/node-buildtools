uniq = (array) ->
  throw 'expected an array' unless array and typeof array.filter is 'function'
  f = (e, i, a) -> a.indexOf(e) is i
  array.filter(f)

module.exports = uniq
