{ promisify } = require('util')
_stat = promisify(require('fs').stat)

stat = (f) ->
  _stat(f).then (s) ->
    s.filename = f
    s

stat.all = (files) ->
  Promise.all files.map(stat)

module.exports = stat
