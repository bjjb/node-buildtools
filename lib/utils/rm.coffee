rimraf = require 'rimraf'

rm = (paths...) ->
  operations = paths.map (path) ->
    new Promise (resolve, reject) ->
      rimraf path, (err) ->
        return reject(err) if err?
        resolve(true)
  Promise.all(operations)

module.exports = rm
