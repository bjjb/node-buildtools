{ promisify } = require 'util'
{ writeFile } = require 'fs'
{ dirname } = require 'path'

writeFile = promisify(writeFile)

mkdir = require './mkdir'

write = (path) ->
  (contents...) ->
    mkdir(dirname(path)).then ->
      content = contents.join("\n")
      writeFile path, content, 'utf8'

module.exports = write
