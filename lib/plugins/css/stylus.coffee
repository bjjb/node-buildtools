{ render } = require 'stylus'

stylus = (options = {}) ->
  (source) ->
    new Promise (resolve, reject) ->
      render source, options, (err, css) ->
        return reject(err) if err?
        resolve css

module.exports = stylus
