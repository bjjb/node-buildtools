# Compile coffeescript

coffeeScript = require 'coffeescript'
flatten = require '../../utils/flatten'
pipe = require '../../pipe'

coffee = (src, options = {}, dest) ->
  [dest, options] = [options, {}] unless dest?
  pipe src, coffee.compile(options), dest

compile = (options = {}) ->
  (input) ->
    opts = Object.assign({}, options)

    if typeof input is 'string'
      return compile(opts)([{ content: input }])

    if typeof input.map is 'function'
      return Promise.all(input.map(compile(opts))).then(flatten)

    { file, content } = input
    throw 'no content' unless content?

    filename = file ? options.filename ? options.file
    Object.assign(opts, { filename })

    new Promise (resolve, reject) ->
      try
        results = []
        result = coffeeScript.compile(content, opts)
        filename = filename?.replace?(/\.coffee$/, '.js')
        { js, sourceMap, v3SourceMap } = result
        if v3SourceMap
          content = v3SourceMap
          file = "#{filename}.map"
          results.push(Object.assign({}, { file, content }))
        else if sourceMap
          content = sourceMap
          file = "#{filename}.map"
          results.push(Object.assign({}, { file, content }))
        content = js ? result
        file = filename
        results.push(Object.assign({}, { file, content }))
        resolve results
      catch e
        reject e

coffee.compile = compile

module.exports = coffee
