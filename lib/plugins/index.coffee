html = require './html'
css  = require './css'
js   = require './js'

{ pug }     = html
{ stylus }  = css
{ coffee }  = js

module.exports = {
  html,
  css,
  js,
  pug,
  stylus,
  coffee,
}

# TODO - postcss, cssnext, babel, cssnano, etc
