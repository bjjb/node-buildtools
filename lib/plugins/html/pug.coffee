{ compile } = require 'pug'

pug = (options = {}) ->
  (source) ->
    compile(source)(options)

module.exports = pug
