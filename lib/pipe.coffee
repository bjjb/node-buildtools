mksrc = require './src'
mkdest = require './dest'
write = require './utils/write'

# A pipeline is a chain of functions, where the result of one is passed to the
# next (left to right). src may be a glob of files, and dest may specify a
# file or a directory.
pipe = (src, tasks..., dest) ->
  src = mksrc(src) if typeof src is 'string'
  dest = mkdest(dest) if typeof dest is 'string'
  p = (args...) -> [src, tasks..., dest].reduce(pipe.reduce)(args...)
  [p.src, p.tasks, p.dest] = [src, tasks, dest]
  p

pipe.reduce = (prev, next) ->
  (args...) ->
    Promise.resolve(prev?(args...) ? prev).then(next)

module.exports = pipe
