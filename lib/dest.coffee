write = require './utils/write'
flatten = require './utils/flatten'
{ join } = require 'path'

fileMapper = (dir) ->
  ({ file, content }) ->
    throw 'expected a file' unless file?
    throw 'no content' unless content?
    write(join(dir, file))(content)

# dest gets a function to write to a file or directory.
dest = (path) ->
  throw(new Error("invalid dest: #{path}")) unless typeof path is 'string'
  f = null
  if path.match(/\/$/)
    f = (files...) ->
      Promise.all(files.map(fileMapper(path)))
    f.multi = true
  else
    f = (files...) ->
      files = flatten(files)
      write(path)(files.map((f) -> f.content ? f).join("\n"))
    f.multi = false
  f

module.exports = dest
