read = require './utils/read'
glob = require './utils/glob'
flatten = require './utils/flatten'
uniq = require './utils/uniq'
stat = require './utils/stat'

# Gets a function which can be used to read from files. It always expects an
# array of strings, and always returns a function which expects to arguments,
# and returns an array of objects with properties { file, content }.
# Globs are expanded, and merged in. Order is preserved. Directories are
# ignored.
src = (globs...) ->
  unless globs.every((g) -> typeof g is 'string')
    throw 'expected an array of strings'

  globMap = (g) -> glob(g)
  flatMap = (list) -> filter(uniq(flatten(list)))

  files = Promise.all(globs.map(globMap)).then(flatMap)
  f = -> files.then (files) -> Promise.all(files.map(map))
  f.files = files
  f

map = (file) ->
  read(file).then (content) ->
    { file, content }

filter = (files) ->
  Promise.all(files.map(stat))
    .then (stats) -> stats.filter((s) -> s.isFile())
    .then (stats) -> stats.map((s) -> s.filename)

module.exports = src
