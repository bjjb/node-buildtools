gaze = require 'gaze'

watch = (pipelines...) ->
  (x...) ->
    pipelines.forEach (pipeline) ->
      if pipeline.glob
        gaze pipeline.glob, (err, watcher) ->
          watcher.on 'all', (event, filepath) -> pipeline(x...)
      if pipeline.dest
        gaze pipeline.glob, (err, watcher) ->
          watcher.on 'deleted', (event, filepath) -> pipeline(x...)

module.exports = watch
