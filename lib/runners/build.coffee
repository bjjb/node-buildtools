build = (pipelines...) ->
  (x...) ->
    Promise.all(pipelines.map((pipeline) -> pipeline(x...)))

module.exports = build
