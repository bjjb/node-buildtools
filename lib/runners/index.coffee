build = require './build'
serve = require './serve'
watch = require './watch'

module.exports = {
  build
  serve
  watch
}
