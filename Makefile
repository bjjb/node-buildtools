.PHONY: all clean test

all:
	coffee -c {test,lib}/*/*/*.coffee
	coffee -c {test,lib}/*/*.coffee
	coffee -c {test,lib}/*.coffee

clean:
	rm -rfv {test,lib}/*/*/*.js
	rm -rfv {test,lib}/*/*.js
	rm -rfv {test,lib}/*.js

test: all
	nyc --reporter=html npm test
