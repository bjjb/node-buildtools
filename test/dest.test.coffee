{ expect, assert } = require 'chai'
{ mkdir, rm, read } = require './helpers'

describe 'dest', ->
  dest = require '../lib/dest'

  before -> mkdir('test/tmp/x')

  after -> rm('test/tmp')

  describe 'dest(file)', ->
    f = dest('test/tmp/x/a')
    it "isn't multi", ->
      expect(f.multi).to.eq(false)
    it 'writes to that file', ->
      p = f('hello')
      p.then ->
        read('test/tmp/x/a').then (x) ->
          expect(x).to.eq('hello')

  [null, undefined, [], {}, ->].forEach (x) ->
    describe "dest(#{JSON.stringify(x)})", ->
      it 'throws an error', ->
        assert.throws (-> dest(x)), 'invalid dest'

  describe 'dest(dir)', ->
    f = dest('test/tmp/x/')
    it 'is multi', ->
      expect(f.multi).to.eq(true)
    it 'writes files to that directory', ->
      p = f({ file: 'a', content: 'foo' }, { file: 'b', content: 'bar' })
      p.then ->
        a = await read('test/tmp/x/a')
        b = await read('test/tmp/x/b')
        expect(a).to.eq('foo')
        expect(b).to.eq('bar')
    it 'complains about invalid files', ->
      [content, file] = [ 'foo', 'bar' ]
      assert.throws (-> f({content})), 'expected a file'
      assert.throws (-> f({file})), 'no content'
