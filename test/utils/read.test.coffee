# Tests the `read` function

{ expect } = require 'chai'
{ mkdir, write, rm } = require '../helpers'

describe 'read', ->
  read = require '../../lib/utils/read'

  before ->
    await mkdir('test/tmp')
    Promise.all(write("test/tmp/#{x}", "#{x}!") for x in ['a', 'b', 'c'])

  after -> rm('test/tmp')

  describe 'read(file)', ->
    it 'resolves to the contents of the file', ->
      expect(await read('test/tmp/a')).to.eq 'a!'
      expect(await read('test/tmp/b')).to.eq 'b!'
      expect(await read('test/tmp/c')).to.eq 'c!'

  describe 'read(nonexistent-file)', ->
    it 'gives back null', ->
      expect(await read('test/tmp/no-such-file')).to.eq null

  describe 'multiple files', ->
    it 'resolves to the contents of all files', ->
      expect(await read('test/tmp/a', 'test/tmp/b')).to.eq "a!\nb!"
      expect(await read('test/tmp/b', 'test/tmp/a')).to.eq "b!\na!"

  describe 'a glob', ->
    it 'resolves to the contents of all files', ->
      expect(await read('test/tmp/*')).to.eq "a!\nb!\nc!"

  describe 'multiple globs', ->
    it 'combines all results', ->
      expect(await read('test/tmp/{a,c}', 'test/tmp/*')).to.eq("a!\nc!\nb!")
