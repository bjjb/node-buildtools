# Tests for the `glob` function

{ expect } = require 'chai'

describe 'glob', ->
  glob = require '../../lib/utils/glob'

  describe 'glob("*")', ->
    it 'returns a promise', ->
      expect(glob('*')).to.be.an.instanceof(Promise)
    it 'resolves to an array', ->
      expect(await glob('*')).to.be.an.instanceof(Array)

  describe 'glob("/no/such/dir")', ->
    it 'resolves to an empty array', ->
      expect(await glob('/no/duch/dir')).to.deep.equal([])
