{ assert } = require 'chai'
{ mkdir, write, exists, rm } = require '../helpers'
unlink = rm

describe 'rm', ->
  rm = require '../../lib/utils/rm'

  before -> mkdir('test/tmp').then -> write('test/tmp/x', 'foo')

  after -> unlink('test/tmp')

  it 'removes all files', ->
    rm('test/tmp/x').then -> exists('test/tmp/x').then assert.isFalse

  it 'deletes directories', ->
    rm('test/tmp').then -> exists('test/tmp').then assert.isFalse

  it "doesn't care if a file doesn't exist", ->
    await rm('test/tmp/nosuchfile')
    await rm('test/tmp/nosuchdir/nosuchfile')
