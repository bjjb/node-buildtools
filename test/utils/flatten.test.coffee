{ expect, assert } = require 'chai'

describe 'flatten', ->
  flatten = require '../../lib/utils/flatten'

  [ null, undefined, 123, {}, 'hello' ].forEach (x) ->
    describe typeof(x), ->
      it 'throws an error', ->
        assert.throws (-> flatten(x)), 'expected an array'

  [ [], [1, 2, 3], ['a', 1, null] ].forEach (x) ->
    describe JSON.stringify(x), ->
      it 'returns unchanged', ->
        expect(flatten(x)).to.deep.equal(x)

  [
    x: [1, [2, [3, [4, 5]]], 6], y: [1, 2, 3, 4, 5, 6]
  ].forEach ({ x, y }) ->
    describe JSON.stringify(x), ->
      it 'flattens', ->
        expect(flatten(x)).to.deep.equal(y)
