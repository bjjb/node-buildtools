{ assert, expect } = require 'chai'
{ rm, mkdir, write } = require '../helpers'

stat = require '../../lib/utils/stat'

describe 'stat', ->
  before ->
    await mkdir('test/tmp/x')
    await write('test/tmp/y', 'hello')

  after -> rm('test/tmp')


  it 'gets the stats of a directory', ->
    stat('test/tmp/x').then (s) ->
      expect(s.isDirectory()).to.eq true
      expect(s.filename).to.eq('test/tmp/x')

  it 'gets the stats of a file', ->
    stat('test/tmp/y').then (s) ->
      expect(s.isDirectory()).to.eq false
      expect(s.filename).to.eq('test/tmp/y')

describe 'stat.all', ->
  before ->
    await mkdir('test/tmp/x')
    await write('test/tmp/y', 'hello')

  after -> rm('test/tmp')

  it 'gets stats for all files', ->
    stat.all(['test/tmp/x', 'test/tmp/y']).then (stats) ->
      [ x, y ] = stats
      expect(x.isFile()).to.eq false
      expect(x.filename).to.eq 'test/tmp/x'
      expect(y.isFile()).to.eq true
      expect(y.filename).to.eq 'test/tmp/y'
