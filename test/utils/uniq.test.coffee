{ assert, expect } = require 'chai'

describe 'uniq', ->
  uniq = require '../../lib/utils/uniq'

  [ null, undefined, {}, 'hello' ].forEach (x) ->
    describe typeof(x), ->
      it 'throws an error', ->
        assert.throws (-> uniq(x)), 'expected an array'

  [
    x: [1, 1, 1, 2, 2, 2, 'a', 'b', 'A'], y: [1, 2, 'a', 'b', 'A']
  ].forEach ({ x, y }) ->
    describe typeof(x), ->
      it 'gets the unique values', ->
        expect(uniq(x)).to.deep.equal(y)
