{ assert, expect } = require 'chai'
{ exists, rm } = require '../helpers'

describe 'mkdir', ->
  mkdir = require '../../lib/utils/mkdir'

  after -> rm('test/tmp')

  it 'returns a promise', ->
    expect(mkdir('test/tmp/foo/bar')).to.be.an.instanceof(Promise)

  it "creates a directory if it doesn't exist", ->
    mkdir('test/tmp/foo/bar').then ->
      exists('test/tmp/foo/bar').then assert

  it "doesn't care if the directory exists", ->
    await mkdir('test/tmp/foo/bar')
    await mkdir('test/tmp/foo/bar')
    expect(await exists('test/tmp/foo/bar')).to.eq(true)

  it 'can make multiple directories', ->
    await mkdir('test/tmp/a', 'test/tmp/b')
    expect(await exists('test/tmp/a')).to.eq(true)
    expect(await exists('test/tmp/b')).to.eq(true)
