# Tests for the `write` function

{ expect } = require 'chai'
{ read, rm } = require '../helpers'

describe 'write', ->
  write = require '../../lib/utils/write'

  after -> rm('test/tmp')

  it 'returns a function', ->
    expect(write('test/tmp/x')).to.be.a 'function'

  it 'writes to the file', ->
    await write('test/tmp/x')('foo')
    expect(await read('test/tmp/x')).to.eq 'foo'
  
  it 'accepts multiple sources', ->
    await write('test/tmp/x')('foo', 'bar', 'baz')
    expect(await read('test/tmp/x')).to.eq 'foo\nbar\nbaz'

