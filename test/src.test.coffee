{ expect, assert } = require 'chai'
{ mkdir, write, rm, read } = require './helpers'

describe 'src', ->
  src = require '../lib/src'

  before ->
    await mkdir('test/tmp')
    await write('test/tmp/a', 'foo')
    await write('test/tmp/b', 'bar')
    await mkdir('test/tmp/x')
    await write('test/tmp/x/a', 'baz')

  after -> rm('test/tmp')

  [123, true, false, {a: 1}, [1, 2], new Date()].forEach (x) ->
    describe JSON.stringify(x), ->
      it 'throws an error', ->
        assert.throws (-> src(x)), 'expected an array of strings'

  [
    'test/tmp/a'
    'test/tmp/b'
    'test/tmp/x/a'
  ].forEach (file) ->
    describe file, ->
      it 'gets a function', ->
        expect(src(file)).to.be.a 'function'
      it 'reads the files name and contents', ->
        read(file).then (content) ->
          expect(await src(file)()).to.deep.eq([{ file, content }])

  multipleFiles = ['test/tmp/a', 'test/tmp/b']
  describe JSON.stringify(multipleFiles), ->
    it 'reads multiple files', ->
      expect(await src(multipleFiles...)()).to.deep.eq [
        { file: 'test/tmp/a', content: 'foo' }
        { file: 'test/tmp/b', content: 'bar' }
      ]
  
  describe 'test/tmp/*', ->
    it 'reads all globbed files', ->
      expect(await src('test/tmp/*')()).to.deep.eq [
        { file: 'test/tmp/a', content: 'foo' }
        { file: 'test/tmp/b', content: 'bar' }
      ]
  describe 'test/tmp/**/*', ->
    it 'reads all globbed files', ->
      expect(await src('test/tmp/**/*')()).to.deep.eq [
        { file: 'test/tmp/a', content: 'foo' }
        { file: 'test/tmp/b', content: 'bar' }
        { file: 'test/tmp/x/a', content: 'baz' }
      ]
