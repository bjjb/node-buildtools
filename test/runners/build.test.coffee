{ expect } = require 'chai'

describe 'build', ->
  build = require '../../lib/runners/build'

  it 'is a function', ->
    expect(build).to.be.a 'function'

  it 'expects pipelines'

  it 'runs the pipelines and writes the outputs'
