{ expect } = require 'chai'

describe 'watch', ->
  watch = require '../../lib/runners/watch'
  
  it 'expects pipelines'
  it 'watches for changes in the pipelines'

  describe 'when a file changes', ->
    it 'builds the appropriate pipeline'
