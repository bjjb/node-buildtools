{ readFile, writeFile, unlink, access } = require 'fs'
rimraf = require 'rimraf'
mktree = require 'mkdir-p'

mkdir = (path) ->
  new Promise (resolve, reject) ->
    mktree path, (err) ->
      return reject(err) if err?
      resolve(true)

write = (f, x) ->
  new Promise (resolve, reject) ->
    writeFile f, x, 'utf8', (e) ->
      return reject(e) if e?
      resolve(true)

read = (f) ->
  new Promise (resolve, reject) ->
    readFile f, 'utf8', (e, x) ->
      return reject(e) if e?
      resolve(x)

exists = (f) ->
  new Promise (resolve, reject) ->
    access f, (err) ->
      return resolve(!err?)

rm = (f) ->
  new Promise (resolve, reject) ->
    rimraf f, (err) ->
      return resolve(!err?)

module.exports = { mkdir, read, write, rm, exists }
