{ expect } = require 'chai'

describe 'pug', ->
  pug = require '../../../lib/plugins/html/pug'

  it 'is a function', ->
    expect(pug).to.be.a('function')

  it 'returns a function', ->
    expect(pug()).to.be.a('function')

  it 'compiles pug to HTML', ->
    expect(pug()('h1 Hi')).to.eq('<h1>Hi</h1>')
