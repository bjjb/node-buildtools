{ expect } = require 'chai'

describe 'stylus', ->
  stylus = require '../../../lib/plugins/css/stylus'

  it 'returns a function', ->
    expect(stylus()).to.be.a('function')

  it 'compiles stylus', ->
    stylus()("p\n color pink").then (css) ->
      expect(css.trim()).to.eq("p {\n  color: #ffc0cb;\n}")
