# Tests for the `coffee` function

{ expect } = require 'chai'
{ read, write, mkdir, rm } = require '../../helpers'

describe 'coffee', ->
  coffee = require '../../../lib/plugins/js/coffee'

  before ->
    await mkdir('test/tmp')
    await write('test/tmp/a.coffee', 'console.log("A")')
    await write('test/tmp/b.coffee', 'console.log("B")')
    await write('test/tmp/c.coffee', 'console.log("C")')

  after -> rm('test/tmp')

  describe '.compile', ->
    it 'returns a function', ->
      expect(coffee.compile()).to.be.a 'function'

  describe '.compile({ bare: true })', ->
    f = coffee.compile(bare: true)
    it 'compiles bare coffeescript', ->
      f('console.log("Hi")').then ([{ content }]) ->
        expect(content).to.eq('console.log("Hi");\n')

  describe '.compile({ bare: true, sourceMap: true })', ->
    f = coffee.compile(filename: 'foo.coffee', bare: yes, sourceMap: yes)
    it 'compiles coffeescript', ->
      f('yes').then (outputs) ->
        expect(outputs.length).to.eq(2)
        [ sourceMap, js ] = outputs
        expect(sourceMap.file).to.eq('foo.js.map')
        expect(JSON.parse(sourceMap.content).version).to.eq(3)
        expect(js.file).to.eq('foo.js')
        expect(js.content).to.eq('true;\n\n')

  describe '.compile()({ file, content })', ->
    f = coffee.compile(bare: yes, sourceMap: true)
    it 'compiles properly', ->
      f(content: 'yes').then ([sourceMap, js]) ->
        expect(js.content).to.eq('true;\n\n')

  describe '.compile()([{ file, content }])', ->
    f = coffee.compile(bare: yes)
    it 'compiles properly', ->
      f([{content:'1',file:'a'},{content:'2',file:'b'}]).then -> (x) ->
        expect(x).to.eq('')

  describe 'coffee(src, dest)', ->
    it 'compiles single files properly', ->
      f = coffee('test/tmp/a.coffee', 'test/tmp/a.js')
      f().then ->
        read('test/tmp/a.js').then (js) ->
          expect(js).to.eq '''
          (function() {
            console.log("A");
          
          }).call(this);

          '''
    it 'compiles multiple files properly', ->
      f = coffee('test/tmp/*.coffee', 'test/tmp/bundle.js')
      f().then ->
        read('test/tmp/bundle.js').then (js) ->
          expect(js).to.eq '''
          (function() {
            console.log("A");

          }).call(this);

          (function() {
            console.log("B");

          }).call(this);

          (function() {
            console.log("C");

          }).call(this);

          '''
