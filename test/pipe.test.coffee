{ expect } = require 'chai'
{ mkdir, rm, read, write } = require './helpers'

describe 'pipe', ->
  pipe = require '../lib/pipe'

  t1 = [
    (x) -> x.toUpperCase()
    (x) -> x.replace('OO', 'ish')
    (x) -> x.split(' ')
    (x) -> x.join('-')
  ]

  t2 = [
    (x) -> x.toLowerCase()
    (x) -> x.split(/\s+/)
    (x) -> JSON.stringify(x)
  ]

  before ->
    await mkdir('test/tmp')
    await write('test/tmp/a.x', '1 A')
    await write('test/tmp/b.x', '1 B')
    await write('test/tmp/c.x', '1 C')
    await write('test/tmp/a.coffee', 'alert "a"')
    await write('test/tmp/b.coffee', 'alert "b"')
    await write('test/tmp/c.coffee', 'alert "c"')

  after -> rm('test/tmp')

  describe 'a valid pipeline (no src or dest)', ->
    p = pipe(t1...)
    it 'is a function', ->
      expect(p).to.be.a('function')
    it 'has a null src', ->
      expect(p.src).to.be.a('function')
    it 'has a null dest', ->
      expect(p.dest).to.be.a('function')
    it 'runs all functions', ->
      expect(await p('foo bar')).to.eq('Fish-BAR')

  describe 'a typical read-work-work-write pipeline', ->
    concat = ((x) -> x.map(({content}) -> content).join("\n"))
    it 'has a functional src', ->
      p = pipe('test/tmp/*.x', concat, t2..., 'test/tmp/x.y')
      expect(p.src).to.be.a 'function'
      expect(await p.src()).to.deep.equal [
        { file: 'test/tmp/a.x', content: '1 A' }
        { file: 'test/tmp/b.x', content: '1 B' }
        { file: 'test/tmp/c.x', content: '1 C' }
      ]
    it 'has a functional dest', ->
      p = pipe('test/tmp/*.x', concat, t2..., 'test/tmp/x.y')
      expect(p.dest).to.be.a 'function'
    it 'runs the pipeline properly', ->
      p = pipe('test/tmp/*.x', concat, t2..., 'test/tmp/x.y')
      p().then ->
        read('test/tmp/x.y').then(JSON.parse).then (array) ->
          expect(array).to.deep.equal ['1','a','1','b','1','c']

  describe 'piping pipelines', ->
    p1 = pipe('test/tmp/a.x', 'test/tmp/a.y')
    p2 = pipe('test/tmp/b.x', 'test/tmp/b.y')
    p3 = pipe('test/tmp/c.x', 'test/tmp/c.y')
    p4 = pipe('test/tmp/*.x', 'test/tmp/d.y')

    it 'runs each pipeline', ->
      ass = require('../lib/src')('test/tmp/a.x')
      expect(ass).to.be.a 'function'
