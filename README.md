# NodeJS Build Tools

These are some functions that I use reasonably frequently to build apps using
Pug, Stylus and Coffee-Script. Here's an example of a useful Cakefile:

```coffee
# Include the build tools
{ 
  html, css, js
  build, serve, watch
} = require '@bjjb/buildtools'

html  = html 'src/index.pug', 'dist/'
css   = css 'src/style.styl', 'dist/'
js    = js 'src/**/*.coffee', 'dist/app.js'

task 'build', 'build the website', build(html, css, js)
task 'start', 'start a dev server', serve(html, css, js)
task 'watch', 'watch for changes', watch(html, css, js)
task 'clean', 'remove built files', clean(html, css, js)
```

Here's the same thing with Gulp

```js
let { gulp, html, css, js, serve, clean } = require('@bjjb/buildtools')
const html = html('src/index.pug, 'dest/'),
      css = css('src/style.styl', 'dest/')
      js = js('src/**/*.coffee', 'dest/app.js')
gulp.task('html', html)
gulp.task('css', css)
gulp.task('js', js)
gulp.task('default', ['html', 'css', 'js'])
gulp.task('watch', gulp.watch([html.filename, css.filename, js.glob])
gulp.task('start', serve(html, css, js))
gulp.task('clean', clean(html, css, js))
```

It's similar to [Grunt][] or [Gulp][], but doesn't try to impose any top-level
CLI on you (the examples above use Cake and Gulp, but plain old JS will work
too). It's not as full-featured as [Webpack][], but it's also easier to get
started.  There's no eco-system here - if you want a plugin, write your own
([it's pretty easy][plugins]). That said, you can use the Webpack plugin to
simplify your configuration.

## Overview

It works pretty simply, by chaining functions together. A _task_ is simply a
function. Generally a task function takes one argument and returns a promise.
If the task receives a function, it resolves it first.

A _pipeline_ is a task made by chaining other tasks together. Pipes also have
some extra properties; they may have `sources`, `destinations` and `tasks`
properties, depending on how they were constructed. If the first argument to
the `pipe` function is a string, it is passed into the `src` function to
ensure that it can read a set of files and pass them to the next task in the
pipeline. If the last argument is a string, is is passed to the `dest`
function, which turns it into a task that knows how to write multiple files to
a location.

Because the tools internally rely on nothing more than functions and promises,
you can plug pretty much anything in as a task. However, because you can
annotate tasks (which pipelines do automatically), special _runner_ functions
can take tasks as an argument, and do fun things with them, like watch sources
for changes and run the task, or start a server which auto-compiles files on
request.

## Batteries included

I wrote it so I wouldn't have to spend any more time than necessary
bootstrapping web apps. So it ships with the tools I use the most:

[Pug][], [Stylus][], [CoffeeScript][], [PostCSS][], [CSSNext][], [SASS][],
[Less][], [Express][], [Webpack][], [Gulp][], [Babel][], [Mocha][], [Chai][],
and possibly a few more. See the [plugin directory][plugdir] for the full
list.

Often, the _plugin_ for these is simply an inclusion of the module, but it
does mean that I only have to `npm install` one package.

## Caveat

This project may be extremely useful for bootstrapping a project, and for
prototyping, but I do **not** recommend it for production projects! You should
hone your build environment like you should all your other code.

[Pug]: https://pugjs.org
[Stylus]: https://stylus-lang.com
[CoffeeScript]: http://coffeescript.org
[Grunt]: https://gruntjs.com
[Gulp]: https://gulpjs.com
[Webpack]: https://webpack.js.org
[PostCSS]: https://postcss.org
[CSSNext]: https://cssnext.io
[Babel]: https://babeljs.io
[Mocha]: https://mochajs.org
[Chai]: https://chaijs.com
[Express]: https://expressjs.com
[Less]: https://lesscss.org
[SASS]: https://sass-lang.com
[plugins]: https://bitbucket.org/bjjb/node-buildtools/wiki/Writing%20Plugins
[plugdir]: https://bitbucket.org/bjjb/node-buildtools/src/master/lib/plugins/
